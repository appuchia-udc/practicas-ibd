FAQ: Uso do servidor Oracle da FIC

    En que consiste a opción do acceso ao servidor Oracle?
    Esta opción consiste en acceder desde o teu equipo a un servidor da UDC no que está funcionando un SXBD Oracle (destinado en exclusiva á docencia), e no que están creadas todas as bases de datos que utilizamos na materia para practicar SQL. O SXBD Oracle está instalado nunha máquina dedicada (10.8.48.29), e funciona 24x7 (agás no caso de situacións excepcionais).
    Para utilizalo, propoñemos o uso de DBeaver, un programa que podedes instalar no voso equipo, e que vos permitirá conectarvos directamente ao servidor Oracle de docencia.
    Esta opción require ter establecida unha conexión VPN coa rede da UDC (excepto) se estades conectados á wifi da FIC.

    Que necesito para utilizar esta opción?
    Para poder acceder a Oracle, necesitas:
    Unha conta de usuario/a de Oracle.
    Unha conexión vpn segura coa rede da UDC.
    Un programa cliente instalado no teu equipo (por exemplo, DBeaver ou DataGrip), que che permita:
    Iniciar unha sesión de traballo co servidor Oracle, usando a túa conta.
    Enviar sentenzas SQL ao servidor Oracle para que as execute.
    Recibir os resultados das sentenzas e presentalos en pantalla.

    Como obteño unha conta de acceso a Oracle?
    Non tes que facer nada. As contas de Oracle créanse automaticamente ao inicio do cuadrimestre para todos os estudantes matriculados/as na materia
    As credencias de acceso a Oracle por defecto son:
    Nome de usuario/a: O teu usuario de servizos.udc.es sen o @udc.es. Exemplo: se o teu usuario/a de servizos.udc.es é nome.de.usuario@udc.es, o teu nome de usuario de Oracle será nome.de.usuario
    Contrasinal: O teu NIF, sen puntos e con letra maiúscula. Exemplo: 12345678Q

    Como establezo unha conexión segura coa rede da UDC?
    A UDC proporciona un servizo de VPN (Virtual Private Network) para poder establecer unha conexión segura desde calquera lugar con acceso a Internet.
    Tedes dispoñible toda a información relevante nesta ligazón.

    Podo conectarme ao servidor Oracle desde casa?
    Si. A única condición para facelo é a de establecer primeiro unha conexión segura por VPN coa rede da UDC.

    É necesario abrir a conexión VPN incluso se estou na FIC?
    Xa non é preciso para conectarte co servidor Oracle (en anos anteriores si que era necesario).

    Por que non me funciona a conexión VPN?
    O acceso por VPN é un servizo ofrecido polos servizos informáticos da UDC. Os docentes da materia non podemos dar soporte ante problemas con este servizo
    Para comunicar problemas ou solicitar axuda, debedes utilizar o portal de AxudaTIC, dispoñible neste enderezo.
    En calquera caso, se usas PulseSecure (que é o software recomendado pola UDC), comproba que cando intentas establecer a conexión a xanela correspondente mostra algo parecido a isto:

    Conexión por VPN

    Que programa cliente necesito instalar no meu equipo para poder probar consultas?
    Existen varias alternativas. Os docentes da materia utilizamos DBeaver. Podes descargalo desde esta ligazón.

    Como me conecto ao servidor Oracle con DBeaver?
    Tes dispoñible un documento coa explicación paso a paso aquí.

    Se decido utilizar outro programa cliente (como por exemplo DataGrip), como me conecto ao servidor Oracle?
    Os parámetros de conexión do servidor son os seguintes:
    Enderezo: 10.8.48.29
    Porto: 1521
    Base de datos/Servizo: docencia
    AVISO: Os docentes da UDC non daremos soporte ao uso de programas que non sexan os recomendados. Se tedes problemas con eles, deberedes resolvelos pola vosa conta.

    Por que non me funciona o meu usuario cando intento acceder a Oracle?
    Lembra que Oracle xestiona as súas propias contas de usuario/a. Comproba que:
    Estas introducindo o teu nome de usuario/a sen @udc.es: nome.de.usuario vs. nome.de.usuario@udc.es
    Estas introducindo o teu nome de usuario/a entre aspas dobres: "nome.de.usuario" vs. nome.de.usuario
    Estás usando o contrasinal do teu usuario de Oracle e non o contrasinal se servizos.udc.es

    Podo cambiar o meu contrasinal de Oracle?
    Si.
    Para cambiar o teu contrasinal, tes que executar unha sentenza como a que se indica na seguinte figura:

    Que fago se non teño usuario de Oracle e necesito acceder urxentemente á base de datos?
    Existe una conta que podedes usar para casos urxentes.
    Os datos da conta provisional son:
    Usuario: scott
    Contrasinal: tiger
    De todas formas, recomendamos que usedes sempre a vosa conta persoal de Oracle. É unha conta compartida, que calquera pode manipular, e corredes o risco de perder puntualmente o acceso ás bases de datos creadas polos docentes da materia.

    Non me funcionan as consultas nas que utilizo datas.

    Para solucionar o problema, executa este comando cada vez que abras unha consola SQL en DBeaver:  ALTER SESSION SET NLS_DATE_FORMAT = 'DD-MM-YYYY HH24.MI.SS';



