SELECT
	DISTINCT 
	'Nombre: ' || emp.ename AS "Nombre",
	emp.job,
	emp.mgr,
	emp.deptno,
	emp.comm,
	emp.sal
FROM
	emp,
	dept
WHERE
	-- IS para filtrar por un nulo
	(comm IS NOT NULL
		OR 1 = 1)
	-- Siempre TRUE
	AND ename LIKE 'M%I%'
	-- % = .* ; _ = .?
ORDER BY
	emp.deptno ASC,
	emp.sal DESC;