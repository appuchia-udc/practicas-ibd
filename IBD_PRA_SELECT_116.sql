-- 1.
--SELECT
--	ename
--FROM
--	emp e
--WHERE
--	sal IN (
--	SELECT
--		max(sal)
--	FROM
--		emp e
--	GROUP BY
--		deptno);
SELECT
	ename
FROM
	emp e
WHERE
	sal IN (
	SELECT
		max(sal)
	FROM
		emp
	WHERE
		deptno = e.deptno);
		
-- 2.
SELECT empno FROM emppro ep WHERE hours = (SELECT max(hours) FROM emppro WHERE empno=ep.empno);

-- 3.
SELECT ename, hours FROM emppro ep JOIN emp e ON e.empno=ep.empno WHERE hours = (SELECT max(hours) FROM emppro WHERE prono=ep.prono);

-- 4.
SELECT
	ename,
	pname,
	hours
FROM
	emppro ep
JOIN emp e ON
	e.empno = ep.empno
JOIN pro p ON
	p.prono = ep.prono
WHERE
	hours = (
	SELECT
		max(hours)
	FROM
		emppro
	WHERE
		prono = ep.prono);

-- 5.
SELECT
	d.dname,
	count(e.empno)
FROM
	emp e
JOIN dept d ON
	d.deptno = e.deptno
WHERE
	e.sal > (
	SELECT
		avg(sal)
	FROM
		emp
	WHERE
		deptno = e.deptno)
GROUP BY
	d.deptno,
	d.dname;

-- 6.
SELECT
	d.dname,
	count(*)
FROM
	emp e
JOIN dept d ON
	d.deptno = e.deptno
WHERE
	e.sal > (
	SELECT
		sal
	FROM
		emp
	WHERE
		empno = e.mgr)
GROUP BY
	d.deptno,
	d.dname;
