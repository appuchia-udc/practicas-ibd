-- 1.
SELECT
	DISTINCT
	emp.deptno,
	emp.job
FROM
	emp
ORDER BY DEPTNO;

-- 2.
SELECT
	DISTINCT
	emp.mgr AS "Cód. empleado"
FROM
	emp
WHERE
	emp.mgr IS NOT NULL
ORDER BY
	emp.mgr;
	
-- 3.
SELECT
	DISTINCT
	pro.loc
FROM
	pro
WHERE
	pro.deptno = 30
ORDER BY
	pro.loc;
	
-- 4.
SELECT
	*
FROM
	emp
WHERE
	emp.mgr IS NULL
ORDER BY
	emp.empno;
	
-- 5.
SELECT
	*
FROM
	emp
WHERE
	emp.mgr IS NOT NULL
	AND (sal + comm > 2500
		OR sal > 2500)
ORDER BY
	emp.empno;
	
-- 6.
SELECT
	*
FROM
	emp
WHERE
	emp.ename LIKE 'S%'
ORDER BY
	emp.ename;

-- 7.
SELECT
	*
FROM
	emp
WHERE
	sal + comm BETWEEN 1500 AND 2500
	OR sal BETWEEN 1500 AND 2500
ORDER BY
	emp.empno;
	
-- 8.
SELECT
	*
FROM
	emp
WHERE
	emp.job IN ('CLERK', 'SALESMAN', 'ANALYST')
	AND (emp.sal + emp.comm > 1250
		OR emp.sal > 1250)
ORDER BY
	emp.empno;
		