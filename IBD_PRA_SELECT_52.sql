-- 1.
SELECT
	count(*),
	sum(COALESCE(sal + comm, sal))
FROM
	emp
WHERE
	emp.job LIKE 'SALESMAN'
	OR emp.job LIKE 'CLERK';

-- 2.
SELECT
	count(comm) AS "Tienen comisión",
	count(*)-count(comm) AS "No tienen comisión",
	AVG(sal) AS "Salario medio",
	avg(comm) AS "Comisión media"
FROM
	emp
	
-- 3.
SELECT
	ename
FROM
	emp
WHERE
	LENGTH(ename) > 5;
	
-- 4.
SELECT
	-- Previamente count(DISTINCT empno) por pensar que un empleado podía estar en varios departamentos
	count(*) AS "Empleados en depts. 20 o 30",
	count(DISTINCT(job)) AS "Trabajos en los depts. 20 y 30"
FROM
	emp
WHERE
	deptno IN (20, 30);
	
-- 5.
SELECT
	count(mgr) AS "Tienen jefe",
	count(DISTINCT mgr) AS "Son jefes",
	count(*)-count(mgr) AS "No tienen jefe"
FROM
	emp;
	
-- 6.
SELECT
	avg(COALESCE(sal + comm, sal))
FROM
	emp
WHERE
	HIREDATE > '01-08-1981'
