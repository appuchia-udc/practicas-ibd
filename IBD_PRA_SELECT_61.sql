-- 1.
SELECT
	deptno,
	count(*) AS "N empleados",
	count(comm) AS "Con comisión",
	count(*)-count(comm) AS "Sin comisión",
	avg(COALESCE(sal + comm, sal)) AS "Ingresos medios"
FROM
	emp
GROUP BY
	deptno
ORDER BY
	deptno;

-- 2. 
SELECT
	DISTINCT deptno
FROM
	emp
WHERE
	comm IS NOT NULL;
	
-- 3.
SELECT
	deptno,
	count(comm) AS "Empleados con comisión",
	COALESCE(avg(comm), 0) AS "Comisión media"
FROM
	emp
GROUP BY
	deptno
ORDER BY
	deptno;
	
-- 4.
SELECT
	deptno,
	count(DISTINCT job) AS "Trabajos desempeñados"
FROM
	emp
GROUP BY
	deptno
ORDER BY
	deptno;
	
-- 5.
SELECT
	deptno, job,
	count(*) AS "Empleados"
FROM
	emp
GROUP BY
	deptno, job
ORDER BY
	deptno;
	
-- 6.
SELECT 
	deptno,
	count(*) AS "Empleados > 2500"
FROM
	emp
WHERE
	COALESCE(sal + comm, sal) > 2500
GROUP BY
	deptno
ORDER BY
	deptno;
