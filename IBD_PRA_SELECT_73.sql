-- 1.
SELECT
	deptno,
	count(*)
FROM
	emp
WHERE
	COALESCE(sal + comm, sal) > 2500
GROUP BY
	deptno
ORDER BY
	deptno;

-- 2. 
SELECT
	deptno,
	count(*)
FROM
	emp
GROUP BY
	deptno
HAVING
	avg(COALESCE(sal + comm, sal)) > 2500
ORDER BY
	deptno
	
-- 3.
SELECT
	deptno
FROM
	emp
WHERE
	job LIKE 'MANAGER'
GROUP BY
	deptno
HAVING
	count(*) > 2
ORDER BY
	deptno;
	
-- 4.
SELECT
	deptno,
	count(*),
	count(comm)
FROM
	emp
GROUP BY
	deptno
HAVING
	count(comm) >= 2
ORDER BY deptno;
	
-- 5.
SELECT
	DISTINCT deptno
FROM
	emp
GROUP BY
	deptno,
	job
HAVING
	count(*) >= 2
ORDER BY
	deptno

