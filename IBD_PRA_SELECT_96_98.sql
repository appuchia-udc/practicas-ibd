-- 1.
SELECT
	p.pname,
	d.deptno
FROM
	pro p
JOIN dept d ON
	p.deptno = d.deptno;

-- 2. 
SELECT
	e.ename,
	ep.prono
FROM
	emp e
JOIN emppro ep ON
	e.empno = ep.empno;
	
-- 3. 
SELECT
	e.ename,
	ep.prono
FROM
	emp e
LEFT JOIN emppro ep ON
	e.empno = ep.empno;

-- 4.
--SELECT

-- 5.
--SELECT

-- 6.
SELECT
	e.ename,
	e.sal,
	j.ename,
	j.sal
FROM
	emp e
JOIN emp j ON
	e.mgr = j.empno
WHERE
	e.sal>j.sal;

SELECT
	e.ename,
	e.sal,
	j.ename,
	j.sal
FROM
	emp e
JOIN emp j ON
	e.mgr = j.empno
	AND e.sal>j.sal;


-- P. 98

-- 2.
SELECT
	dname,
	count(ename)
FROM
	emp e
JOIN dept d ON
	e.deptno = d.deptno
GROUP BY
	d.deptno,
	dname

-- 3.
SELECT
	j.ename,
	count(e.empno)
FROM
	emp e
JOIN emp j ON
	e.mgr = j.empno
GROUP BY
	j.ename;

-- 4.
SELECT
	p.pname,
	sum(hours)
FROM
	pro p
JOIN emppro ep ON
	p.prono = ep.prono
GROUP BY
	p.pname
HAVING
	sum(ep.hours) > 15;

-- 5.
SELECT
	d.dname,
	count(p.prono)
FROM
	dept d
JOIN pro p ON
	d.deptno = p.deptno
GROUP BY
	d.dname
HAVING
	count(p.prono) > 2;

-- 6.
SELECT
	d.dname
FROM
	dept d
JOIN emp e ON
	e.deptno = d.deptno
GROUP BY
	d.deptno,
	d.dname,
	e.job
HAVING
	count(e.empno) > 2;

-- 7.
SELECT
	d.dname,
	count(e.empno)
FROM
	emp e
RIGHT JOIN dept d ON
	e.deptno = d.deptno
GROUP BY
	d.deptno,
	d.dname;

-- 8.
SELECT
	e.empno,
	COALESCE(sum(hours), 0)
FROM
	emp e
LEFT JOIN emppro ep ON
	e.empno = ep.empno
GROUP BY
	e.empno,
	e.ename;

-- 9.
SELECT
	j.ename,
	count(e.empno)
FROM
	emp e
RIGHT JOIN emp j ON
	e.mgr = j.empno
	-- NO en un HAVING o WHERE 
	AND e.sal > j.sal
GROUP BY
	j.empno,
	j.ename;


-- P. 106

-- 1.
SELECT
	e.empno,
	e.ename,
	e.sal
FROM
	emp e
WHERE
	sal > (
	SELECT
		avg(sal)
	FROM
		emp);
		
-- 2.
SELECT
	d.dname,
	count(*)
FROM
	emp e
RIGHT JOIN dept d ON
	d.deptno = e.deptno
	AND e.sal > (
	SELECT
		avg(sal)
	FROM
		emp)
GROUP BY
	d.deptno,
	d.dname;
	
-- 3.
SELECT j.ename FROM emp j JOIN emp e ON e.mgr=j.empno GROUP BY j.ename;
SELECT e.ename FROM emp e WHERE e.empno IN (SELECT e.mgr FROM emp e);

-- 4.
SELECT e.ename FROM emp e WHERE e.empno NOT IN (SELECT e.mgr FROM emp e WHERE e.mgr IS NOT NULL);

-- 5.
SELECT e.ename FROM emp e WHERE e.sal >= ALL (SELECT e.sal FROM emp e);

-- 6.
SELECT
	d.dname,
	sum(e.sal)
FROM
	dept d
JOIN emp e ON
	e.deptno = d.deptno
GROUP BY
	d.deptno,
	d.dname
HAVING
	sum(e.sal) >= ALL (
	SELECT
		sum(sal)
	FROM
		dept d
	JOIN emp e ON
		e.deptno = d.deptno
	GROUP BY
		d.deptno);
		
SELECT
	dname,
	sum(sal)
FROM
	emp e
JOIN dept d ON
	e.deptno = d.deptno
GROUP BY
	d.deptno,
	dname
HAVING
	sum(sal) >= ALL (
	SELECT
		sum(sal)
	FROM
		emp
	GROUP BY
		deptno);
		
-- 7.
