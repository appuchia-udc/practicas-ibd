-- 1.
SELECT
	*
FROM
	EMP
WHERE
	EMP.COMM / EMP.SAL > .5

-- 2.
SELECT
	*
FROM
	emp
WHERE
	COALESCE(emp.comm, 0) <= 0.25 * emp.sal
ORDER BY
	empno;

-- 3.
SELECT
	*
FROM
	emp
WHERE
	mgr IS NULL
ORDER BY
	empno;

-- 4.
SELECT
	ename,
	sal / comm
FROM emp
WHERE
	comm != 0
ORDER BY
	ename;

-- 5.
SELECT
	*
FROM
	emp
WHERE
	mgr > empno
	AND (sal BETWEEN 1000 AND 2000
		OR deptno = 30);

-- 6.
SELECT
	ename,
	sal,
	comm,
	COALESCE(sal + comm, sal) AS "Salario total"
FROM
	emp
WHERE
	COALESCE(sal + comm, sal) > 2300;

-- 7.
SELECT
	deptno,
	count(DISTINCT job)
FROM
	emp
GROUP BY
	deptno;

-- 8.
SELECT
	MAX(sal),
	sum(comm),
	count(*)
FROM
	emp; 

-- 9.
SELECT max(e.ename) FROM emp e;

-- 10.
SELECT max(sal), min(sal), max(sal) - min(sal) FROM emp;

-- 11.
SELECT count(job), count(empno), count(sal), sum(sal) FROM emp e WHERE e.deptno=30;

-- 12.
SELECT count(e.empno) FROM emp e WHERE e.deptno = 20;

-- 13.
SELECT count(e.comm) FROM emp e;

-- 14.
SELECT e.job, count(e.empno) FROM emp e GROUP BY e.job;

-- 15.
SELECT e.deptno, sum(sal) FROM emp e GROUP BY e.deptno;

-- 16.
SELECT p.deptno, count(p.prono) FROM pro p GROUP BY p.deptno;

-- 17.
SELECT ep.prono, sum(ep.hours), count(ep.empno) FROM emppro ep GROUP BY ep.prono HAVING count(ep.empno) >= 3;

-- 18.
SELECT deptno, loc, count(*) FROM pro GROUP BY loc, deptno;

-- 19.
SELECT deptno, count(*) FROM emp WHERE sal > 1500 GROUP BY deptno;

-- 20.
SELECT deptno, count(e.empno) FROM emp e GROUP BY deptno HAVING min(sal) > 1000;

-- 21.
SELECT deptno, job FROM emp GROUP BY job, deptno HAVING count(*) > 2;

-- 22.
SELECT * FROM emp WHERE sal > (SELECT sal FROM emp WHERE empno=7934) ORDER BY sal;

-- 23.
SELECT empno, ename, loc FROM emp e JOIN dept d ON d.deptno=e.deptno AND loc IN ('DALLAS', 'NEW YORK');

-- 24.
SELECT * FROM emp WHERE sal >= (SELECT avg(sal) FROM emp);

-- 25.
SELECT * FROM emp WHERE deptno=10 AND job IN (SELECT job FROM emp e JOIN dept d ON e.deptno=d.deptno WHERE dname LIKE 'SALES');

-- 26.
SELECT * FROM emp e WHERE empno IN (SELECT mgr FROM emp) ORDER BY ename DESC;

-- 27.
SELECT * FROM emp e WHERE empno NOT IN (SELECT mgr FROM emp WHERE mgr IS NOT NULL) ORDER BY ename DESC;

-- 28.
SELECT * FROM dept d WHERE deptno NOT IN (SELECT deptno FROM emp WHERE deptno IS NOT NULL);

-- 29.
SELECT empno FROM emppro ep WHERE hours = (SELECT max(hours) FROM emppro WHERE ep.prono=prono)

-- 30.
SELECT
	ename,
	deptno
FROM
	emp a
WHERE
	sal > (
	SELECT
		max(sal)
	FROM
		emp
	WHERE
		deptno = a.deptno
		AND empno != a.empno)

-- 31.
SELECT * FROM emp e WHERE sal = (SELECT max(sal) FROM emp WHERE job=e.job);

-- 32.
SELECT empno, ename, loc FROM emp e JOIN dept d ON e.deptno=d.deptno WHERE LENGTH(loc) > 6 ORDER BY loc DESC, ename;

-- 33.
SELECT e.ename, p.pname FROM emppro ep JOIN emp e ON e.empno=ep.empno JOIN pro p ON p.prono=ep.prono ORDER BY ename;

-- 34.
SELECT d.dname, p.pname FROM dept d LEFT JOIN pro p ON p.deptno=d.deptno ORDER BY d.dname;

-- 35.
SELECT e.ename "Empleado", j.ename "Jefe" FROM emp e LEFT JOIN emp j ON e.mgr=j.empno;

-- 36.
SELECT e.ename, e.HIREDATE, j.ename, j.HIREDATE  FROM emp e JOIN emp j ON e.mgr=j.empno AND e.HIREDATE < j.HIREDATE 

-- 37.
SELECT d.dname, ep.empno FROM dept d JOIN pro p ON p.deptno=d.deptno JOIN emppro ep ON ep.prono=p.prono;

-- 38.
SELECT e.empno, e.sal, ep.prono, ep.hours FROM emp e JOIN emppro ep ON e.empno=ep.empno ORDER BY empno;

-- 39.
SELECT d.dname, count(*), avg(sal) FROM dept d JOIN emp e ON e.deptno=d.deptno GROUP BY d.deptno, d.dname;

-- 40.
SELECT d.dname, sum(sal) FROM dept d JOIN emp e ON e.deptno=d.deptno AND e.sal > (SELECT avg(sal) FROM emp e) GROUP BY d.deptno, d.dname

-- 41.
SELECT
	p.prono,
	loc,
	count(*) "Proyectos",
	max(hours) "Horas máx.",
	min(hours) "Horas mín.",
	max(hours)-min(hours) "Diff."
FROM
	pro p
JOIN emppro ep ON
	p.prono = ep.prono
WHERE
	deptno = 30
GROUP BY
	p.prono,
	loc

-- 42.
SELECT 

-- 43.
SELECT 

-- 44.
SELECT 

-- 45.
SELECT 

-- 46.
SELECT 

-- 47.
SELECT 

-- 48.
SELECT 

-- 49.
SELECT 

-- 50. ?
--SELECT j.empno, j.ename, count(empno) FROM emp e RIGHT JOIN emp j ON e.mgr=j.empno GROUP BY j.empno, j.ename
SELECT j.empno, j.ename, count(empno) FROM emp e LEFT JOIN emp j ON e.mgr=j.empno GROUP BY j.empno, j.ename

-- 51. 
SELECT d.deptno, d.dname, sum(sal) FROM emp e JOIN dept d ON e.deptno=d.deptno GROUP BY d.deptno, d.dname HAVING sum(sal) = (SELECT max(sum(sal)) FROM emp e GROUP BY deptno)

-- 52.
SELECT a.ename, a.sal FROM emp a WHERE 2 > (SELECT count(*) FROM emp WHERE sal>a.sal)

-- 53.
SELECT 

-- 54.
SELECT empno, ename, sum(hours) FROM emp e LEFT JOIN emppro ep  ON e.empno=ep.empno GROUP BY e.empno, ename

-- 55.
SELECT
	dname, count(empno)
FROM
	dept d
JOIN emp e ON
	d.deptno = e.deptno
	AND e.empno IN (
	SELECT
		mgr
	FROM
		emp)
GROUP BY
	d.deptno,
	d.dname
HAVING
	count(empno) >= all(SELECT count(empno) FROM emp WHERE empno IN (SELECT mgr FROM emp) GROUP BY deptno);

-- 56.
SELECT 

-- 57.
SELECT 

-- 58.
SELECT 

-- 59.
SELECT 

-- 60.
SELECT 

-- 61.
SELECT 

-- 62.
SELECT 

-- 63.
SELECT 

-- 64.
SELECT 

-- 65.
SELECT 

-- 66.
SELECT 

-- 67.
SELECT
	ename,
	job
FROM
	emp
WHERE
	empno IN (SELECT mgr FROM emp)
	AND NOT exists(SELECT * FROM emp s WHERE mgr=s.empno AND job!=s.job);

-- 68.
SELECT
	d.dname,
	count(DISTINCT x.empno) AS "Jefes",
	count(e.empno) AS "Subordinados"
FROM
	(emp e
JOIN emp x ON
	e.mgr = x.empno)
RIGHT JOIN dept d ON
	d.deptno = x.deptno
GROUP BY
	d.deptno,
	d.dname;

-- 69.
SELECT d.deptno, dname FROM dept d JOIN (emp e) ON e.deptno=d.deptno GROUP BY d.deptno, dname HAVING count(e.empno) > 3

-- 70.
SELECT 


SELECT * FROM emp e JOIN emp j ON j.empno=e.mgr