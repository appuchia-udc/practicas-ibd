
-- 27
-- 1.
SELECT dname, count(e.empno) empleados FROM emp e RIGHT JOIN dept d ON d.deptno=e.deptno GROUP BY d.deptno, d.dname

-- 2.
SELECT e.ename, coalesce(sum(ep.hours),0) FROM emp e LEFT JOIN emppro ep ON e.empno=ep.empno GROUP BY e.empno, e.ename

-- 3.
SELECT e.ename, j.ename jefe, dname FROM emp e LEFT JOIN (emp j JOIN dept d ON j.deptno=d.deptno) ON e.mgr=j.empno

-- 4.
SELECT e.ename, count(DISTINCT loc) FROM emp e LEFT JOIN (emppro ep JOIN pro p ON p.prono=ep.prono) ON e.empno=ep.empno GROUP BY e.empno, e.ename

-- 5.
SELECT
	pname,
	dname,
	count(DISTINCT empno)
FROM
	dept d
RIGHT JOIN (pro p
	JOIN emppro ep ON
		ep.prono = p.prono)
	ON
	p.deptno = d.deptno
WHERE
	d.deptno IN (30, 40)
GROUP BY
	p.prono,
	pname,
	dname;

--SELECT * FROM pro p JOIN dept d ON p.deptno=d.deptno WHERE d.deptno IN (30, 40);

-- 29
-- 1.
SELECT
	j.ename,
	count(e.ename)
FROM
	emp e
RIGHT JOIN emp j ON
	e.mgr = j.empno
	AND to_char(e.hiredate, 'YYYY')=to_char(j.hiredate, 'YYYY')
WHERE
	j.empno IN (
	SELECT
		mgr
	FROM
		emp)
GROUP BY
	j.empno,
	j.ename;

-- 2.
SELECT
	DISTINCT ename
FROM
	emp e
JOIN dept d ON
	e.deptno = e.deptno
JOIN emppro ep ON
	ep.empno = e.empno
JOIN pro p ON
	ep.prono = p.prono
	AND p.loc = d.loc

-- 3.
SELECT
	ename,
	count(p.loc)
FROM
	emp e
JOIN dept d ON
	e.deptno = e.deptno
JOIN emppro ep ON
	ep.empno = e.empno
JOIN pro p ON
	ep.prono = p.prono
	AND p.loc = d.loc
GROUP BY
	e.empno,
	ename;

-- 4.
SELECT
	ename,
	count(p.loc)
FROM
	emp e
LEFT JOIN dept d ON
	e.deptno = d.deptno
LEFT JOIN emppro ep ON
	ep.empno = e.empno
LEFT JOIN pro p ON
	ep.prono = p.prono
	AND p.loc = d.loc
GROUP BY
	e.empno,
	ename;

-- 35
-- 1.
SELECT empno, ename, sal, (SELECT ROUND(avg(sal), 2)  FROM emp) "Salario medio" FROM emp WHERE sal > (SELECT avg(sal) media FROM emp);

-- 2.
SELECT empno, ename, sal, media "Salario medio" FROM emp e JOIN (SELECT deptno, round(avg(sal),2) media FROM emp GROUP BY deptno) d ON e.deptno=d.deptno;

-- 3.
SELECT prono, ename, hours FROM emp
NATURAL JOIN emppro
WHERE (prono, hours)= SOME(
    SELECT
        prono,
        max(hours)
    FROM
        emppro
    GROUP BY
        prono
);

-- 4.
--SELECT prono, ename, hours FROM emp
--NATURAL JOIN emppro
--WHERE (loc, hours)= SOME(
--    SELECT
--        loc,
--        hours
--    FROM
--        emppro
--    GROUP BY
--        prono
--);
-- ?

-- 5.
SELECT avg(medias) FROM (SELECT sum(hours) medias FROM emppro GROUP BY prono);

-- 6.
SELECT pname, sum(hours) suma FROM pro NATURAL JOIN emppro GROUP BY prono, pname having sum(hours) > (SELECT avg(medias) FROM (SELECT sum(hours) medias FROM emppro GROUP BY prono));

-- 37
-- 1.
SELECT ename, maximo-sal FROM emp e JOIN (SELECT deptno, max(sal) maximo FROM emp GROUP BY deptno) m ON m.deptno=e.deptno;

-- 2.
SELECT deptno, sum(sal), (SELECT max(suma) FROM (SELECT sum(sal) suma FROM emp GROUP BY deptno))-sum(sal) diff FROM emp e GROUP BY deptno;

-- 3.
SELECT DISTINCT empno, ename, loc FROM emp NATURAL LEFT JOIN emppro NATURAL LEFT JOIN pro;

-- 4.
SELECT empno, loc FROM emppro CROSS JOIN (SELECT DISTINCT loc FROM pro) WHERE (empno, loc) NOT IN (SELECT empno, loc FROM emppro NATURAL JOIN pro);

-- 5.
SELECT ename, loc FROM emp CROSS JOIN (SELECT DISTINCT loc FROM pro) WHERE (empno, loc) NOT IN (SELECT empno, loc FROM emppro NATURAL JOIN pro) ORDER BY ename;

--6.

