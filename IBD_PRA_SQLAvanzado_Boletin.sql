-- 1.
SELECT email FROM alumno WHERE email NOT IN (SELECT email FROM MEN_FORO WHERE email IS NOT NULL);

-- 6.
SELECT
    cod_c "Curso",
    numero "Edicion",
    count(email) "Registros"
FROM
    EDICION
NATURAL JOIN REXISTRASE
GROUP BY
    COD_C,
    NUMERO
HAVING
    count(email) >= ALL(
        SELECT
            count(email)
        FROM
            REXISTRASE r
        WHERE
            r.COD_C = COD_C
        GROUP BY
            COD_C,
            NUMERO
    );
    
--SELECT COD_C, numero, count(email) FROM EDICION NATURAL JOIN REXISTRASE GROUP BY COD_C, numero ORDER BY COD_C ;

-- 26.
--SELECT
--    c.COD_C, c.NOME, e.NUMERO, e.PREZO, r.email
--FROM
--    CURSO c
--LEFT JOIN EDICION e  ON e.COD_C = c.COD_C AND e.PREZO > 100 
--JOIN REXISTRASE r ON r.COD_C = c.COD_C AND r.NUMERO = e.NUMERO
--ORDER BY c.COD_C, e.NUMERO;

SELECT
    c.COD_C,
    c.NOME,
    count(e.NUMERO),
    (
        SELECT
            count(email)
        FROM
            REXISTRASE r
        WHERE
            r.COD_C = c.COD_C
    ) alumnos
FROM
    CURSO c
LEFT JOIN EDICION e ON
    e.COD_C = c.COD_C
    AND e.PREZO > 100
GROUP BY
    c.COD_C,
    c.NOME;
    
-- 27.
SELECT
    e.COD_C,
    e.NUMERO,
    (
        SELECT
            count(*)
        FROM
            TEMA t
        WHERE
            t.COD_C = e.COD_C
            AND t.NUMERO = e.NUMERO
    ) temas,
    (
        SELECT
            COUNT(*)
        FROM
            REXISTRASE r
        WHERE
            r.COD_C = e.COD_C
            AND r.NUMERO = e.NUMERO
    ) alumnos
FROM
    EDICION e
JOIN CURSO c ON c.COD_C = e.COD_C 
GROUP BY
    e.COD_C,
    e.NUMERO;


-- 35.
SELECT
    a.email,
    round(avg(prezo), 2) "Precio medio",
    round(avg(prezo) - (
        SELECT
            avg(prezo)
        FROM
            ALUMNO a
        JOIN REXISTRASE r ON
            a.EMAIL = r.EMAIL
        JOIN EDICION e ON
            r.COD_C = e.COD_C
            AND r.NUMERO = e.NUMERO
        WHERE
            a.ORGANIZACION LIKE 'ABUELA'
    ), 2) "Diferencia"
FROM
    ALUMNO a
JOIN REXISTRASE r ON
    a.EMAIL = r.EMAIL
JOIN EDICION e ON
    r.COD_C = e.COD_C
    AND r.NUMERO = e.NUMERO
GROUP BY
    a.email,
    a.ORGANIZACION;


-- 30.
SELECT
    e.COD_C,
    e.NUMERO,
    p.NSS,
    p.NOME,
    p.DATA_ALTA - (
        SELECT
            MIN(DATA_ALTA)
        FROM
            PROFESOR p2
        JOIN CAPACITADO c ON
            c.NSS = p.NSS
        WHERE
            e.COD_C = c.COD_C
    ) "Diff (dias)"
FROM
    EDICION e
JOIN PROFESOR p ON
    e.nss = p.nss;

-- 31.
SELECT
    UNIQUE a.NOME,
    c.NOME,
    'SI' "Matr."
FROM
    ALUMNO a
LEFT JOIN REXISTRASE r ON
    r.EMAIL = a.EMAIL
LEFT JOIN CURSO c ON
    c.COD_C = r.COD_C
ORDER BY a.NOME, c.NOME;

-- ?
