# IBD

[![Author](https://img.shields.io/badge/Project%20by-Appu-9cf?style=flat-square)](https://gitlab.com/appuchia)

## Qué es esto

Aquí está el código que he escrito en las clases de Introducción a las Bases de Datos.

## Licencia

El código está licenciado bajo la [licencia GPLv3](https://gitlab.com/appuchia/appuchia-udc/ibd-practicas/-/blob/master/LICENSE).

Coded with 🖤 by Appu
